# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users = User.create([
 {
  name: 'Kirito',
  username: 'kirito',
  email: 'kirito@mail.ru',
  avatar_url: 'https://i.pinimg.com/originals/83/91/9e/83919e6297f7fbf5083d19947c662fe7.jpg',
  password:'123',
  password_confirmation: '123'
 },
 {
  name: 'Asuna',
  username: 'asuna',
  email: 'asuna@mail.ru',
  avatar_url: 'https://avatarfiles.alphacoders.com/995/thumb-1920-99555.png',
  password:'321',
  password_confirmation: '321'
 },
 {
  name: 'Yo',
  username: 'asakura_yo',
email: 'asakura@mail.ru',
  avatar_url: 'https://i.pinimg.com/originals/93/e0/ac/93e0ac47bc82a6aee146f0f687332bb5.jpg',
  password: '456',
  password_confirmation: '456'
 }
])
