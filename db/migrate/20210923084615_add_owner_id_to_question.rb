class AddOwnerIdToQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :owner_id, :integer
    add_index :questions, :owner_id
  end
end
