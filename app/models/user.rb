require 'openssl' 

class User < ApplicationRecord
  ITERATIONS = 20000
  DIGEST = OpenSSL::Digest::SHA256.new

  has_many :questions, dependent: :destroy
  
  validates :email, :username, presence: true
  validates :email, :username, uniqueness: true, on: [:create, :update]
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP}, on: [:create, :update]
  validates :username, length: {maximum: 40}, format:{ with: /\A[a-zA-Z\d_]+\z/}

  attr_accessor :password

  mount_uploader :avatar_url, AvatarUploader

  validates_presence_of :password, on: :create # <=> validates :password, presence: true
  validates_confirmation_of :password

  before_save :encrypt_password, :username_to_downcase
  def encrypt_password
    if self.password.present?
      #создаем т. н. соль - рандомная строка, усложнаяющая задачу хакерам
      self.password_salt = User.hash_to_string(OpenSSL::Random.random_bytes(16))
      #создаем хеш пароля - длинная уникальная строка из которой невозможно восстановить исходный пароль
      self.password_hash = User.hash_to_string(OpenSSL::PKCS5.pbkdf2_hmac(self.password, self.password_salt, ITERATIONS, DIGEST.length, DIGEST))
    end
  end

  # служебный метод, преобразует бинарную строку в 16ный формат, для удобства хранения
  def self.hash_to_string(password_hash)
    password_hash.unpack('H*')[0]
  end

  def self.authenticate(email, password)
    user = find_by(email: email)
    if user.present? && user.password_hash == User.hash_to_string(OpenSSL::PKCS5.pbkdf2_hmac(password, user.password_salt, ITERATIONS, DIGEST.length, DIGEST))
      user
    else
      nil
    end
  end

  def username_to_downcase
     self.username.downcase!
  end
 end
